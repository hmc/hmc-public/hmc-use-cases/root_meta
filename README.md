# ROOTmeta

This project investigates approaches to improve the human and machine readability of [ROOT files](https://root.cern/). It originates from the [Particle Physics Metadata](https://codebase.helmholtz.cloud/hmc/hmc-public/hmc-use-cases/a4-metadata) project.

## Additional _readme_ branch

It is recommended to add a separate _readme_ branch which helps a user to assess and access the content of the ROOT file. The _readme_ branch contains the following core elements addressing different kinds of metadata:

- Bibliographic Metadata: Addressing general information about the ROOT file such as title, topic, or contributors.
- Domain specific Metadata: Addressing physical parameters concerning the creating of data such as beam probe or beam energy involved in a measurement.
- Interoperability Metadata: Addressing technical requirements to access the data such as a suitable operating system, a ROOT software version, or the arrangement of data described in xsd.

<img src="./docs/structure.jpg" width="500">

The _readme_ branch is explained in the [Readme Schema](https://codebase.helmholtz.cloud/hmc/hmc-public/hmc-use-cases/root_meta/-/blob/main/docs/readme_schema.md) and more formally defined by the [readme_schema.xsd file](https://codebase.helmholtz.cloud/hmc/hmc-public/hmc-use-cases/root_meta/-/blob/main/docs/readme_schema.xsd).

## Software

As a proof of concept, two Python code snippets are included for adding a _readme_ branch to a ROOT file and recreating code from the _readme_ branch.

### RootAnnotator

The [RootAnnotator](https://codebase.helmholtz.cloud/hmc/hmc-public/hmc-use-cases/root_meta/-/blob/main/src/python/root_annotator.py) is a Python code which creates a _readme_ branch from the [readme.json file](https://codebase.helmholtz.cloud/hmc/hmc-public/hmc-use-cases/root_meta/-/blob/main/input_data/A4/readme.json). Here, the code is included in the _readme.json_ file as strings (not created from the files), see e.g. _customCodeFile\_item1_.

An example is provided for data of the A4 Instrument in [input_data/A4](https://codebase.helmholtz.cloud/hmc/hmc-public/hmc-use-cases/root_meta/-/blob/main/input_data/A4/) (without _readme_ branch) and the annotated ROOT file and recreated files from the _readme_ branch in [output/A4](https://codebase.helmholtz.cloud/hmc/hmc-public/hmc-use-cases/root_meta/-/blob/main/output_data/A4/). The annotation with the _readme_ branch increases the A4 ROOT file size by about 100 KB (0.5%).

### RootExtractor

The Python code [RootExtractor](https://codebase.helmholtz.cloud/hmc/hmc-public/hmc-use-cases/root_meta/-/blob/main/src/python/root_extractor.py) recreates code that is stored in the _readme_ branch of a ROOT file. Concerning the A4 example, the files _TMedusa.cxx_, _TMedusa.h_, _Makefile_, and _structure.xsd_ are recreated from the _readme_ branch of the _medusa_run78850.root_ file (see [output_data/A4](https://codebase.helmholtz.cloud/hmc/hmc-public/hmc-use-cases/root_meta/-/blob/main/output_data/A4/)). 

## Next Meeting

not yet planned - depends on progress, no agenda yet, Zoom: to be announced by email, please contact gerrit.guenther@helmholtz-berlin.de to be added to the mailing list (same as for [Particle Physics Metadata](https://codebase.helmholtz.cloud/hmc/hmc-public/hmc-use-cases/a4-metadata)).

## Last Meeting
08:30 (CET) March 22nd, 2024, see [minutes](https://codebase.helmholtz.cloud/hmc/hmc-public/hmc-use-cases/root_meta/-/blob/main/docs/minutes/20240322_minutes.md).
