#ifndef ROOT_TMedusa
#define ROOT_TMedusa

#include "TObject.h"
#include "TROOT.h"
//#include <iostream.h>
#include "TH1.h"
#include "TH2.h"

#include <math.h> // 19.04.2008 J.D.

#define Medusa_Memsize 0x20000 // 128 KByte raw data
#define Full_Memsize 0x40000 // 256 KByte raw data

class TMedusa: public TNamed
    {
    private:
        UInt_t memval[Medusa_Memsize]; // contains two 2-dimensional 64 x 256 histograms
        // (for the two polarisations ...)
        // Unint_t: unsigned integer, 4 bytes
        // <-> memval[2][64][256] <-> memval[pol][y][x] = memval[pol*64*256+y*256+x] 
        Int_t counter;
        Int_t valley, edge, peak, sigma;

    public:
        TMedusa();
        TMedusa(char *name, char *title);
        ~TMedusa();
        void SetVal(UInt_t val);
        UInt_t GetVal();
        void Reset();
        Int_t ReadFromFile(char *filename);
        Int_t ReadFromFile(FILE *datei);
        Int_t WriteToFile(char *filename);
        void Fill2D(TH2D **histpol0, TH2D **histpol1);
        void ProjectX(TH1D **histpol0, TH1D **histpol1, Int_t ymin, Int_t ymax);
        void ProjectY(TH1D **histpol0, TH1D **histpol1, Int_t xmin, Int_t xmax);
        void FindEdgePeak();
        Int_t SwapPola();
        Int_t GetPeak() {return peak;}
        Int_t GetEdge() {return edge;}
        Int_t GetSigma() {return sigma;}
        Int_t GetValley() {return valley;}
        ClassDef(TMedusa,1)
    };

#endif
