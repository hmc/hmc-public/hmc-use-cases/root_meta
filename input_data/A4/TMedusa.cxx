/* class TMedusa.cxx
provides the storage of raw binary data within the ROOT framework
objects of class TMedusa can be written into a ROOT file
by invoking the method Write() inherited from TObject, e.g.

=========================================================================================

TMedusa *medusa;
TFile Rootfile("example.root","RECREATE","example file for storing TMedusa object");
medusa=new TMedusa();
...
medusa->Write();
Rootfile.Close();

==========================================================================================

Institut fuer Kernphysik Mainz
08.10.1999
Update 20.06.2000

*/

#include "TObject.h"
#include "TROOT.h"
#include "TMedusa.h"
#include "TNamed.h"

void medianfilter(double feld[], int laenge, int breite);
void ableiten(double feld[], int laenge);
int findeNull(double ableit[], double feldy[], double ableit2[], int laenge, int *b2, int entries, int *vl);
int findeMinimum(double feldy[], int feldx[], int laenge);
void glaetten(double feld[], int laenge, double sigma);
void kleinviehcheck(double feld[], int laenge);
void logarith(double feld[], int laenge);

ClassImp(TMedusa)

TMedusa::TMedusa() {
    // default contructor
    }

TMedusa::TMedusa(char *name, char *title) {
    // contructor providing title and name
    counter=0; peak=0; edge=0; sigma=0; valley=0;
    SetName(name);
    SetTitle(title);
    }

TMedusa::~TMedusa() {
    // default destructor
    }

void TMedusa::SetVal(UInt_t val) {
    // store a new Int_t into the raw data
    memval[counter]=val;
    if (counter<Medusa_Memsize) counter++;
    }

UInt_t TMedusa::GetVal() {
    // read one UInt_t out of the raw data
    UInt_t retval;
    retval=memval[counter];
    if (counter<Medusa_Memsize) counter++;
    return retval;
    }

void TMedusa::Reset() {
    // reset the raw data. Next UInt_t written or read will be at the first position
    counter=0;
    }

Int_t TMedusa::ReadFromFile(FILE *datei) {
    size_t success;

    success = fread(&memval,Medusa_Memsize, 1, datei); // reads in 128kByte
    if (success!=1) return 2; // error reading file
    return 0;
    }

Int_t TMedusa::ReadFromFile(char *filename) {
    // reads 128KBytes from filepointer datafile
    // returns 0 in case of success, 1 if file could not be opened, 2 in other cases of failure

    FILE *datafile;
    size_t success;

    if ((datafile=fopen(filename,"rb"))==NULL) {
        return 1; // error opening file
        }

    success = fread(&memval,Medusa_Memsize, 1, datafile); // reads in 128kByte
    fclose (datafile);
    if (success!=1) return 2; //error reading file
    return 0;
    }

Int_t TMedusa::WriteToFile(char *filename) {
    // writes data to 128kByte binary datafile

    FILE *datafile;
    size_t success;
    if ((datafile=fopen(filename,"wb"))==NULL) {
        return 1; // error opening file
        }
    success = fwrite(&memval,Medusa_Memsize, 1, datafile); // writes 128kByte
    fclose (datafile);
    if (success!=1) return 2; //error reading file
    return 0;
    }

void TMedusa::Fill2D(TH2D **histpol0, TH2D **histpol1) {
    char titel0[256], titel1[256], name0[256], name1[256];
    int x,y;

    sprintf(titel0,"%s pol0",GetTitle());
    sprintf(titel1,"%s pol1",GetTitle());
    sprintf(name0,"%s_p0",GetName());
    sprintf(name1,"%s_p1",GetName());
    *histpol0=new TH2D(titel0,name0,256,-0.5,255.5,64,-0.5,63.5);
    *histpol1=new TH2D(titel1,name1,256,-0.5,255.5,64,-0.5,63.5);
    Reset();
    for (y=0;y<64;y++) {
        for (x=0;x<256;x++) {
            (*histpol0)->Fill(x,y,GetVal());
            }
        }
    for (y=0;y<64;y++) {
        for (x=0;x<256;x++) {
            (*histpol1)->Fill(x,y,GetVal());
            }
        }
    }

void TMedusa::ProjectX(TH1D **histpol0, TH1D **histpol1, Int_t ymin, Int_t ymax) {
    char titel0[256], titel1[256], name0[256], name1[256];
    int x,y, index;
    Double_t sum0, sum1;

    sprintf(titel0,"%s pol0 ProjX",GetTitle());
    sprintf(titel1,"%s pol1 ProjX",GetTitle());
    sprintf(name0,"%s_p0_px",GetName());
    sprintf(name1,"%s_p1_px",GetName());

    if (ymax==64) {
        sprintf(name0,"%s_p0_px1",GetName());
        sprintf(name1,"%s_p1_px1",GetName());
        }
    if (*histpol0!=NULL) {delete *histpol0; *histpol0=NULL;}
    if (*histpol1!=NULL) {delete *histpol1; *histpol1=NULL;}
    *histpol0=new TH1D(name0,titel0,256,-0.5,255.5);
    *histpol1=new TH1D(name1,titel1,256,-0.5,255.5);
    if (ymin<0) ymin=0; if (ymax>63) ymax=63;
    for (x=0;x<256;x++) {
        sum0=0; sum1=0;
        for (y=ymin;y<=ymax;y++) {
            index=y*256+x;
            sum0=sum0+memval[index];
            sum1=sum1+memval[16384+index];
            }
        (*histpol0)->Fill(x,sum0);
        (*histpol1)->Fill(x,sum1);
        }
    }

void TMedusa::ProjectY(TH1D **histpol0, TH1D **histpol1, Int_t xmin, Int_t xmax) {
    char titel0[256], titel1[256], name0[256], name1[256];
    int x,y, index;
    Double_t sum0, sum1;

    sprintf(titel0,"%s pol0 ProjY",GetTitle());
    sprintf(titel1,"%s pol1 ProjY",GetTitle());
    sprintf(name0,"%s_p0_py",GetName());
    sprintf(name1,"%s_p1_py",GetName());

    *histpol0=new TH1D(name0,titel0,32,31.5,63.5);
    *histpol1=new TH1D(name1,titel1,32,31.5,63.5);
    if (xmin<0) xmin=0; if (xmax>255) xmax=255;
    for (y=32;y<64;y++) {
        sum0=0; sum1=0;
        for (x=xmin;x<=xmax;x++) {
            index=y*256+x;
            sum0=sum0+memval[index];
            sum1=sum1+memval[16384+index];
            }
        (*histpol0)->Fill(y,sum0);
        (*histpol1)->Fill(y,sum1);
        }
    }

void TMedusa::FindEdgePeak() {
    // find edge, peak and sigma of spectrum and store values internally
    // values can be accessed by methods GetEdge(), GetPeak() and GetSigma()

    int i, elast, x,y, breite, index, entries, val;
    int xw[512];
    double yw[512], hw[512], y2w[512], hw2[512];
    double summe, summe1;
    unsigned long sum0, sum1;

    // fill arrays with projection data
    entries=0;
    for (x=0;x<256;x++) {
        sum0=0; sum1=0;
        for (y=32;y<64;y++) { // betrachte nur obere 32 Bins der Energie im Zentralkristall
            index=y*256+x; // 1-di.m Adresse des Bins im 2-dim. Array; pol0
            sum0=sum0+memval[index];
            sum1=sum1+memval[16384+index]; // pol1
            }
        xw[x]=x; // Array der x-Werte (Energiesumme-Bin)
        yw[x]=sum0; // Array der y-Werte (Summer der Ereignisse im Energiesumme-Bin für pol0)
        hw[x]=sum0; // "Sicherungskopie" der Werte von yw
        entries=entries+sum0+sum1; // Gesamtzahl der Ereignisse im Histogram
        }

    summe=0; for (i=0;i<=254; i++) {summe=summe+yw[i];}
    if (summe==0) {edge=-1;} /* ist das Spektrum leer, so is nix Kante */
    else {
        summe1=0; for (i=246; i<=254; i++) {summe1=summe1+yw[i];}

        if (summe1/summe>0.0002) /* Check: Ist das Spektrum uebersteuert, d.h. liegt die Kante gar nicht mehr innerhalb der 256 kanaele? */
            {edge= 255;}
        else {
            kleinviehcheck(yw, 254);
            medianfilter(yw, 254, 21); /* Glaette das Spektrum mit Fensterbreite 21*/
            logarith(yw, 254);
            ableiten(yw, 254);
            glaetten(yw,254,13); /* Glaette jetzt auch noch die Ableitung... */
            ableiten(yw, 254);
            edge=findeMinimum(yw,xw,254);
            }

        /* Kante ist gefunden, nun stelle das Spektrum wieder her... */
        for (i=1;i<255;i++) {yw[i]=hw[i];}

        /******************** Jetzt beginnt die Peak-Suche *******************************/
        glaetten(yw, 254, 2); /* Glaette das Spektrum gaussisch */
        for (i=0;i<=255;i++) hw2[i]=yw[i]; /* Kopiere das geglaettete Spektrum nach hw2 */

        ableiten(yw, 254);
        glaetten(yw,254,4); /* Glaette jetzt auch noch die Ableitung... */
        for (i=0;i<=254;i++) y2w[i]=yw[i]; /* Kopiere die geglaettete Ableitung nach y2w */

        ableiten(y2w,254);
        glaetten(y2w,254,2);

        elast=findeNull(yw,hw2,y2w,254, &breite, entries,&val);
        peak=elast; valley=val;

        /* Bestimme nun Breite des elastischen Peaks und seine Eintragzahlen rechts vom Peak und links vom Peak */
        if (elast==-1) {sigma=-1;}
        else {
            sigma=elast; while ((hw2[sigma]/hw2[elast]>0.60653)&&(sigma<255)) {sigma++;}
            if ((sigma<255)&&((sigma-elast)>0)) {sigma=sigma-elast;} else {sigma=-1;}
            }
        }
    }

Int_t TMedusa::SwapPola() {
    // vertauscht die Daten für pol0 und pol1
    // getested -> funktioniert! (CW 13.3.2009)
    int offset=256*64; // = 16384
    int size=offset*4; // = Medusa_Memsize/2
    UInt_t *buf=new UInt_t[size];
    if (!buf) {printf("Error: couldn't allocate memory to swap pol0/pol1 data!\n"); return 1;}
    // printf("Swapping memory locations %p and %p.\n",memval,memval+offset); // später auskommentieren!
    // printf("Pointer difference in bytes is %d.\n",(memval+offset)-memval); // später auskommentieren!
    memcpy(buf,memval,size);
    memcpy(memval,memval+offset,size); // alternativ: memcpy(memval,&(memval[offset]),size);
    memcpy(memval+offset,buf,size); // alternativ: memcpy(&(memval[offset]),buf,size);

    delete buf;
    return 0;
    }

int findeMinimum(double feldy[], int feldx[], int laenge) {
    /* sucht nach absolutem Minimum im feldy und gibt die entsprechenden x-Werte aus */
    int anzahl, start, gesamt, i, durch,index;
    int merke[256];
    anzahl=0; for (i=0;i<=255;i++) merke[i]=0;
    for (i=253;i>0;i--) {
        if ((feldy[i]*feldy[i+1]<0)&&(feldy[i]*feldy[i+2]<0)) {
            if (feldy[i+1]>0) {
                if (feldy[i+1]<-feldy[i]) {durch=feldx[i+1];} else {durch=feldx[i];}
                anzahl++; merke[anzahl]=durch;
                }
            }
        }
    if (anzahl>0) {
        index=1;
        start=index; while (merke[index]-merke[index-1]<3) index--;
        gesamt=0; for (i=index; i<=start; i++) gesamt=gesamt+merke[i];
        gesamt=gesamt/(start-index+1);
        }
    else {gesamt=-1;}
    return(gesamt);
    }

int findeNull(double ableit[], double feldy[], double ableit2[], int laenge, int *b2, int entries, int *vl) {
    /* sucht nach Nulldurchgaengen in feldy, der ersten Ableitung */
    /* ueberprueft anhand der zweiten Ableitung, ob positiver Peak vorliegt */
    /* checkt die Peakbreite (sollte zw. 6 und 19 liegen */
    /* liefert die Kanalnr. des elast. Peak zurueck */
    /* falls nicht gefunden, liefert -1 zurueck */
    /* liefert in per reference uebergebenen Parameter b2 die Breite zurueck */

    int granz, anzahl, start, breite, gesamt, i, index;
    int merke[256], elast[256], elastbreite[256];
    double vhlt;

    granz=0; anzahl=0; for (i=0;i<=255;i++) merke[i]=0;
    for (i=25;i<=laenge-2;i++) {
        if ((ableit[i]*ableit[i+1]<0)&&(ableit[i-1]*ableit[i+2]<0)) {
            anzahl++; merke[anzahl]=i;
            }
        }
    gesamt=-1; elastbreite[0]=-1;
    while (anzahl>0) {
        index=anzahl;
        start=index; while (merke[index]-merke[index-1]<3) index--;
        gesamt=0; for (i=index; i<=start; i++) gesamt=gesamt+merke[i];
        gesamt=gesamt/(start-index+1);
        breite=-1;
        if (ableit2[gesamt]<=0) {
            i=gesamt; while ((i<=laenge)&&(feldy[i]>0.6321*feldy[gesamt])) {i++;}
            breite=i-gesamt;
            }
        if (feldy[gesamt]>0) {vhlt=entries/feldy[gesamt];} else {vhlt=5000;}
        if ((breite>3)&&(breite<35)&&(vhlt<5000)) {elast[granz]=gesamt; elastbreite[granz]=breite; granz++;}
        anzahl=anzahl-(start-index+1);
        if ((anzahl>0)&&(granz==1)) *vl=merke[anzahl];
        }
    if (granz>0) {gesamt=elast[0];} else {gesamt=-1;}
    if (((gesamt-(*vl))>70)||(((*vl)-gesamt)>70)) *vl=-1;
    *b2=elastbreite[0];
    return(gesamt);
    }

void ableiten(double feld[], int laenge) {
    /* leitet Double-Array mit laenge Eintraegen ab */

    int i;
    double hilf[500];

    for (i=1;i<=laenge-1;i++) {
        hilf[i]=feld[i]-feld[i-1];
        }

    for (i=1;i<=laenge-1;i++) {
        feld[i]=hilf[i];
        }
    feld[0]=feld[1]; feld[laenge]=feld[laenge-1];
    }

void kleinviehcheck(double feld[], int laenge) {
    /* sucht nach "Dreck"effekten im Spektrum und setzt selbige auf Null*/

    int i;
    double summe;
    summe=0;
    for (i=0;i<=laenge;i++) {
        summe=summe+feld[i];
        }
    for (i=0;i<=laenge;i++) {
        if (feld[i]<summe/5000) feld[i]=0;
        }
    }

void logarith(double feld[], int laenge) {
    /* logarithmiert das Spektrum */

    int i;
    for (i=0;i<=laenge;i++) {
        if (feld[i]>0) {feld[i]=10000*log(feld[i]);}
        }
    }

void medianfilter(double feld[], int laenge, int breite) {
    /* Medianfilter; glaettet Integer-Array mit laenge Eintraegen, Fensterbreite breite */

    int nr, mittel, b2,i,t,j;
    double hilf[512], hilf2[512];
    double hilf3;

    b2=breite/2;
    for (i=b2+1;i<laenge-b2-1;i++) {
        mittel=0; nr=0;
        for (t=i-b2;t<=i+b2;t++) {hilf2[nr]=feld[t]; nr++;}
        for (t=0;t<nr; t++) {
            for (j=t;j<nr; j++) {
                if (hilf2[j]<hilf2[t]) {hilf3=hilf2[j]; hilf2[j]=hilf2[t]; hilf2[t]=hilf3;}
                }
            }
        hilf[i]=hilf2[b2];
        }

    for (i=0; i<b2+1; i++) hilf[i]=hilf[b2+1];
    for (i=laenge-b2-1; i<laenge; i++) hilf[i]=hilf[laenge-b2-2];
    for (i=0;i<=laenge;i++) {feld[i]=hilf[i];} /* geglaettetes Feld zurueckliefern */
    }

void glaetten(double feld[], int laenge, double sigma) {
    /* glaettet Double-Array mit laenge Eintraegen mit Gaussfilter, Breite sigma */

    int anfang, ende, viersigma,i,t;
    double hilf[500];
    double normfaktor,y;

    normfaktor=0.39894228/sigma; viersigma=(int) (4*sigma);
    for (i=0;i<laenge;i++) {
        y=0;
        anfang=i-viersigma; if (anfang<0) (anfang=0);
        ende=i+viersigma; if (ende>=laenge) {ende=laenge-1;}
        for (t=anfang;t<=ende;t++) {
            y=y+normfaktor*exp(-0.5*(t-i)*(t-i)/sigma/sigma)*feld[t];
            }
        hilf[i]=y;
        }
    for (i=0;i<=laenge;i++) {feld[i]=hilf[i];} /* geglaettetes Feld zurueckliefern */
    }
