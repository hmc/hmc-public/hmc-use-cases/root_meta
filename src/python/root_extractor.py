# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT


import json
import os
import shutil
import subprocess
import uproot


class RootExtractor(object):
    """
    Extracts information from a Readme directory of a Root file.
    Note: self.mount_path is the directory used in docker to mount volumes.
    """

    def __init__(self, input_path, input_file, output_path):
        """
        Initializes class variables.
        :param input_path: String containing the path to the directory of the ROOT file that is to be processed.
        :param input_file: String containing the name of the ROOT file that is to be processed.
        :param output_path: String containing the path to the directory that is used to extract metadata from the ROOT
               file.
        :return: None.
        """
        self.file_path = input_path
        self.file_name = input_file
        self.output_path = output_path
        self.mount_path = "myroot"
        self.metadata_file_name = "metadata.json"
        self.docker_file_name = "docker-compose.yaml"
        self.shell_script_name = "run.sh"
        self.make_file_name = "Makefile"
        self.cmake_file_name = "CMakefile"
        self.structure_file_name = "structure.xsd"
        self.manual_file_name = "manual.txt"
        self.start_docker = False

        pass

    def __str__(self):
        """
        Prints class information.
        :return:
        """
        text_str = "File path: " + self.file_path
        text_str += ", file name: " + self.file_name

        return text_str

    def process_file(self):
        """
        Opens a ROOT file, extracts information from the Readme branch, and recreates customized C++ code files if
        available.
        :return: None.
        """
        self.copy_file()
        readme = self.read_readme()
        self.write_metadata(readme)
        self.recreate_files(readme)
        self.create_docker_script(readme)
        self.create_docker_image()

        pass

    def read_readme(self):
        """
        Reads a readme branch at the top-level of the ROOT file and returns its content as a dictionary.
        :return: Dictionary containing the readme branch.
        """
        metadata_dict = {}
        # open ROOT file and find readme branch (without version, e.g. readme;1 -> readme)
        with uproot.open(os.path.join(self.file_path, self.file_name)) as root_file:
            for key, value in root_file.items():
                if key.split(";")[0] == "readme":
                    # remove version, e.g. ;1
                    for sub_key, sub_value in root_file[key].items():
                        metadata_dict[sub_key.split(";")[0]] = sub_value

        return metadata_dict

    def write_metadata(self, readme_dict):
        """
        Writes metadata of the readme branch to metadata.json file.
        :param readme_dict: Dictionary containing the readme branch of the ROOT file.
        :return: None.
        """
        self.write_file(os.path.join(self.output_path, self.metadata_file_name), json.dumps(readme_dict))

        pass

    def recreate_files(self, readme_dict):
        """
        Recreates the customized code files (and others) from the readme branch at the same location as input file
        (self.file_path).
        :param readme_dict: Dictionary containing the readme branch of the ROOT file.
        :return: None.
        """
        # find customCode and corresponding customCodeFileName for each pair,
        # e.g. customCode_item1 and customCodeFileName_item1
        for key in readme_dict.keys():
            file_name = ""
            # exclude customCodeFileName keys
            if key == "customCodeFile" or key.startswith("customCodeFile_"):
                if "_" in key:
                    code_key, suffix = key.split("_", 1)
                    name_key = code_key + "Name" + "_" + suffix
                else:
                    name_key = key + "Name"
                if name_key in readme_dict:
                    file_name = readme_dict[name_key]
            elif key == "makeFile":
                file_name = self.make_file_name
            elif key == "cmakeFile":
                file_name = self.cmake_file_name
            elif key == "structureXsd":
                file_name = self.structure_file_name
            elif key == "manual":
                file_name = self.manual_file_name

            if not file_name == "":
                self.write_file(os.path.join(self.output_path, file_name), readme_dict[key])

        pass

    def create_docker_script(self, readme_dict):
        """
        Writes a docker compose file and calls creation of a shell script if operatingSystemDocker is given.
        :param readme_dict: Dictionary containing the readme branch of the ROOT file.
        :return: None.
        """
        if "operatingSystemDocker" in readme_dict:
            with open("../../docker/docker-compose-template.yaml", "r") as f:
                docker_script = f.read()
            docker_script = docker_script.replace("<operatingSystemDocker>", readme_dict["operatingSystemDocker"])
            docker_script = docker_script.replace("<rootFile>", self.file_name)
            docker_script = docker_script.replace("<mountPath>", self.mount_path)
            docker_script = docker_script.replace("<shellScriptName>", self.shell_script_name)
            # docker_script = self.add_files2docker(docker_script, readme_dict)
            self.write_file(os.path.join(self.output_path, self.docker_file_name), docker_script)
            self.create_shell_script(readme_dict)
            self.start_docker = True

        pass

    def create_shell_script(self, readme_dict):
        """
        Writes the shell script run.sh to be called by the docker compose file.
        :param readme_dict: Dictionary containing the readme branch of the ROOT file.
        :return: None.
        """
        if "dockerShellScript" in readme_dict:
            shell_script = readme_dict["dockerShellScript"]
        else:
            # shell_script = "sudo snap install root-framework\n"
            shell_script = "#!/bin/bash\n"
            shell_script += "# run.sh\n\n"
            shell_script += "sudo apt update\n"
            shell_script += "sudo apt install -y cpp\n"
            shell_script += ""
            shell_script += "sudo apt install -y root-system\n"
            shell_script += "sudo apt install -y make\n"
            shell_script += "cd " + self.mount_path + "\n"
            print(readme_dict)
            # if "makeFile" in readme_dict:
            #     shell_script += "make\n"
            # elif "cmakeFile" in readme_dict:
            #     shell_script += "cmake\n"

            # shell_script += "./root.sh\n"
            # shell_script += "root " + self.file_name + "\n"
            # shell_script += "root -l <<-EOF\n"
            # for command in self.get_root_commands(readme_dict):
            #     shell_script += readme_dict[command] + "\n"

            # shell_script += "TBrowser b\n"
            # shell_script += "EOF\n"
        self.write_file(os.path.join(self.output_path, self.shell_script_name), shell_script)

        pass

    def add_files2docker(self, docker_script, readme_dict):
        """
        Adds files to as volumes to the docker compose file.
        :param docker_script: String containing the docker compose script.
        :param readme_dict: Dictionary containing the readme branch of the ROOT file.
        :return: String (containing the modified docker compose script).
        """
        for key in readme_dict.keys():
            prefix = self.get_prefix(key)
            if prefix == "customCodeFileName":
                docker_script = self.add_volume2docker(readme_dict[key], docker_script)
            elif prefix == "makeFile":
                docker_script = self.add_volume2docker(self.make_file_name, docker_script)
            elif prefix == "cmakeFile":
                docker_script = self.add_volume2docker(self.cmake_file_name, docker_script)

        return docker_script

    def add_volume2docker(self, file_name, docker_script):
        """
        Adds a file name to the volume section of the docker compose file.
        :param file_name: String containing the name of a file to be added to the script.
        :param docker_script: String containing the docker compose script.
        :return: String (containing the modified docker compose script).
        """
        up_to_command, command_part = docker_script.rsplit("command:", 1)
        volume_line = "  - ./" + file_name + ":/" + self.mount_path + "/" + file_name + "\n"
        return up_to_command + volume_line + "    command:" + command_part

    def get_root_commands(self, readme_dict):
        """
        Collects and sorts rootCommands that are defined in readme_dict.
        :param readme_dict: Dictionary containing the readme branch of the ROOT file.
        :return: List of strings (containing rootCommands).
        """
        command_list = []
        for key in readme_dict.keys():
            if self.get_prefix(key) == "rootCommand":
                command_list.append(key)

        return command_list

    def get_prefix(self, key):
        """
        Returns the prefix of the key (part before first '_').
        :param key: String containing a key of the readme_dict, e.g. 'customCodeFileName_item1'.
        :return: String (containing the prefix of the key, e.g. 'customCodeFileName').
        """
        prefix, suffix = self.split_key(key)
        return prefix

    @staticmethod
    def split_key(key_string):
        """
        Separates prefixes and possible suffixes and returns both.
        :param key_string: String containing the key including suffixes, e.g. customCodeFile_item1.
        :return: Tuple of strings containing prefixes (e.g. customCodeFile) and suffixes (e.g. _item1).
        """
        if "_" in key_string:
            prefix, suffix = key_string.split("_", 1)
            suffix = "_" + suffix
        else:
            prefix = key_string
            suffix = ""

        return prefix, suffix

    @staticmethod
    def write_file(file_path, content):
        """
        Writes the content to a file which is located at file_path.
        :param file_path: String containing the path to the file to be written (including file name).
        :param content: String containing the content of the file.
        :return: None.
        """
        with open(file_path, "w", newline='\n') as f:
            f.write(content)
        pass

    def create_docker_image(self):
        """
        Starts docker if corresponding docker image is provided and tries to open the ROOT file within the container.
        :return: None.
        """
        if self.start_docker:
            subprocess.run(["docker", "start", os.path.join(self.output_path, "docker-compose.yaml")])
        pass

    def copy_file(self):
        """
        Copies the ROOT file from self.input_path to self.output_path.
        :return: None.
        """
        if not os.path.exists(self.output_path):
            os.makedirs(self.output_path)
        shutil.copy2(os.path.join(self.file_path, self.file_name), os.path.join(self.output_path, self.file_name))
        pass


# standard entry point
if __name__ == "__main__":
    input_path = os.path.join("D:", os.path.sep, "projects", "ROOTmeta", "output_data", "A4")
    input_file = "medusa_run78850.root"

    root = RootExtractor(input_path, input_file)
    root.process_file()
    del root

    pass
