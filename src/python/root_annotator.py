# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum Berlin (HZB)
# SPDX-License-Identifier: MIT


import datetime
import pytz
import json
import os
import shutil
import uproot

from root_extractor import RootExtractor


class RootAnnotator(object):
    """
    Adds a Readme directory to top level of a Root file.
    """

    def __init__(self, input_path, input_file, output_path):
        """
        Initializes class variables.
        """
        self.input_path = input_path
        self.input_file = os.path.join(input_path, input_file)
        self.output_file = os.path.join(output_path, input_file)
        self.output_path = output_path

        pass

    def __str__(self):
        """
        Prints some class information.
        :return:
        """
        text_str = "Input file: " + self.input_file
        text_str += ", output file: " + self.output_file
        text_str += ", output path: " + self.output_path

        return text_str

    def process_file(self):
        """
        Copies a ROOT file and modifies that copy by adding a Readme directory with various metadata describing the
        ROOT file.
        :return: None.
        """
        self.copy_file()
        self.create_readme()
        self.print_readme()

        pass

    def create_readme(self):
        """
        Creates a Readme directory at the top-level of the ROOT file and returns the handle to the  directory.
        :return: None.
        """
        _path = os.path.join("..", "..", "input_data", "A4")
        metadata_dict = RootAnnotator.read_json(_path, "readme.json")

        # open ROOT file to modify it
        with uproot.update(self.output_file) as root_file:
            print("original: ", root_file.keys())

            # create Readme directory in ROOT file
            readme = root_file.mkdir("readme")

            # add content to Readme directory
            self.add_metadata(metadata_dict, readme)

        return

    def add_metadata(self, _dict, readme_dir):
        """
        Adds content from a dictionary to a ROOT file. Depending on the value type sub-structures may be created.
        Values of type str are added as plain data.
        :param _dict: Dictionary containing the metadata to be added to the Readme directory.
        :param readme_dir: Uproot handle to the Readme directory of a ROOT file.
        :return: None.
        """
        # add current date as dateCreated
        readme_dir["dateCreated"] = self.get_utc(datetime.datetime.now())
        for key, value in _dict.items():

            if isinstance(value, str):

                if value.startswith("fromFile:"):
                    file_name = value.replace("fromFile:", "")
                    if self.find_file(self.input_path, file_name):
                        with open(os.path.join(self.input_path, file_name), "r") as f:
                            file_content = f.read()
                        readme_dir[key] = file_content
                        # add file_name as customCodeFileName if corresponding key doesn't exist
                        key_prefix, key_suffix = self.split_key(key)
                        key_name = key_prefix + "Name" + key_suffix
                        if key_prefix == "customCodeFile" and key_name not in _dict:
                            readme_dir[key_name] = file_name
                else:
                    readme_dir[key] = str(value)

            elif isinstance(value, float):
                readme_dir[key] = str(value)
            elif isinstance(value, int):
                readme_dir[key] = str(value)
            else:
                print("Don't know what to do with ", str(key))

        pass

    @staticmethod
    def split_key(key_string):
        """
        Separates prefixes and possible suffixes and returns both.
        :param key_string: String containing the key including suffixes, e.g. customCodeFile_item1.
        :return: Tuple of strings containing prefixes (e.g. customCodeFile) and suffixes (e.g. _item1).
        """
        if "_" in key_string:
            prefix, suffix = key_string.split("_", 1)
            suffix = "_" + suffix
        else:
            prefix = key_string
            suffix = ""

        return prefix, suffix

    @staticmethod
    def get_utc(dt):
        """
        Converts a local datetime object into a string representing the UTC time with corresponding timezone shift;
        includes summer and winter time shift.
        :param dt: Datetime object representing a local time.
        :return: String representing UTC time.
        """
        timezone = pytz.timezone("Europe/Berlin")
        timezone_date = timezone.localize(dt, is_dst=None)
        part = str(timezone_date).split("+")
        utc_shift = part[1]
        time = timezone_date.strftime("%Y-%m-%dT%H:%M:%S") + "+" + utc_shift
        return str(time)

    @staticmethod
    def read_json(_path, _filename):
        """
        Reads a JSON file and returns its content as a dictionary.
        :return: Dictionary.
        """
        metadata_dict = {}
        if RootAnnotator.find_file(_path, _filename):
            _file_path = os.path.join(_path, _filename)
            with open(_file_path) as json_file:
                metadata_dict = json.load(json_file)

        return metadata_dict

    @staticmethod
    def find_file(folder_path, filename):
        """
        Checks if a file in a given path exists and returns True or False.
        :param folder_path: String containing the path to a folder, e.g. 'D:/A4-metadata/input_exampleData'.
        :param filename: String containing the name of a file, e.g. 'logbook_whitelist.json'.
        :return: Boolean that is 'True' if the file in the folder exists; otherwise 'False'.
        """
        file_found = False
        for file in os.listdir(folder_path):
            if file.startswith(filename):
                file_found = True
                break
        return file_found

    def copy_file(self):
        """
        Copies the ROOT file from self.input_path to self.output_path.
        :return: None.
        """
        shutil.copy2(self.input_file, self.output_file)
        pass

    def print_readme(self):
        """
        Prints the content of the added Readme directory of the modified ROOT File.
        :return: None.
        """
        # open file as read only
        with uproot.open(self.output_file) as root_file:
            print("modified: ", root_file.keys())
            print("readme directory:")
            for key, value in root_file["readme"].items():
                if isinstance(value, str) and len(value) > 90:
                    print("   ", str(key), ":", value[0:10], "...")
                else:
                    print("   ", str(key), ":", value)

        pass


# standard entry point
if __name__ == "__main__":
    input_path = os.path.join("D:", os.path.sep, "projects", "ROOTmeta", "input_data", "A4")
    output_path = os.path.join("D:", os.path.sep, "projects", "ROOTmeta", "output_data", "A4")
    input_file = "medusa_run78850.root"

    root = RootAnnotator(input_path, input_file, output_path)
    root.process_file()
    del root

    extraction_path = os.path.join(output_path, "extracted")
    root = RootExtractor(output_path, input_file, extraction_path)
    root.process_file()
    del root

    pass
