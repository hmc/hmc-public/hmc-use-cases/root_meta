# Readme Schema
The readme section of a ROOT file should give guidance to the user what kind of data the file contains and how it is arranged. The provided metadata can be classified according to the following sections.

## 1. Bibliographic Metadata
This metadata provides general information about the creators of the data, an abstract of ist scientific context, or the date it was generated. This kind of information is community-agnostic and usually covered be general metadat schemas such as [DataCite](https://schema.datacite.org/meta/kernel-4.5/).

| No. | Name           | Cardinality    | Format        | Description                                                                                                                                                                                                                                                                                                              |
|-----|----------------|----------------|---------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1.1 | thisSchema     | 0/1 (optional) | string        | A description of the structure of this schema and meaning of its elements.                                                                                                                                                                                                                                               |
| 1.2 | dateCreated    | 0/1 (optional) | ISO Date/Time | The date in ISO 8601 format, when the ROOT file was initially created.                                                                                                                                                                                                                                                   |
| 1.3 | rights         | 0/1 (optional) | string        | The name of a policy that describes restrictions concerning the (re-)use of the ROOT file.                                                                                                                                                                                                                        |
| 1.4 | subject        | 0+ (optional)  | string        | One or more topics the ROOT file is related to, e.g. from [ModSci](https://saidfathalla.github.io/Science-knowledge-graph-ontologies/doc/ModSci_doc/ontology.rdf), [PaNET](https://github.com/ExPaNDS-eu/ExPaNDS-experimental-techniques-ontology/blob/master/source/PaNET.owl), or [IBA](https://doi.org/10.1088/1741-4326/ab5817). |
| 1.5 | title          | 0+ (optional)  | string        | A name or title that the ROOT file belongs to, e.g. the title of an experiment series or a proposal title.                                                                                                                                                                                                               |
| 1.6 | contributor    | 0+ (optional)  | string        | A name of an institution or a person responsible for the ROOT file.                                                                                                                                                                                                                                                      |
| 1.7 | wasGeneratedBy | 0/1 (optional) | string        | A name of an activity or instrument which generated the ROOT file.                                                                                                                                                                                                                                                       |

## 2. Matter Specific Metadata
This section provides metadata which is specific for the research field Matter and is intended for scientists of the corresponding comunities to classify the ROOT file's content.


| No.  | Name                  | Cardinality    | Format        | Description                                                                                                                                                                                                                                                                                                                 |
|------|-----------------------|----------------|---------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 2.1  | digitalObjectCategory | 0+ (optional)  | string        | A name providing an arbitrary classification of the ROOT file's content, e.g. [measurement data](https://purls.helmholtz-metadaten.de/hob/HDO_00001071), [computational data](https://purls.helmholtz-metadaten.de/hob/HDO_00001069), or [software](http://purl.obolibrary.org/obo/IAO_0000010).                            |
| 2.2  | dateStart             | 0/1 (optional) | ISO Date/Time | The date in ISO 8601 format when a process (measurement or computation) started that creates the ROOT file.                                                                                                                                                                                                                 |
| 2.3  | dateEnd               | 0/1 (optional) | ISO Date/Time | The date in ISO 8601 format when a process (measurement or computation) ceases that creates the ROOT file.                                                                                                                                                                                                                  |
| 2.4  | beamProbe             | 0+ (optional)  | string        | The type of probe that was used to measure or simulate the data of the ROOT file, e.g. neutron, x-ray, muon, electron, ultraviolet, visible light, positron, proton, ion, infrared.                                                                                                                                         |
| 2.5  | beamEnergy            | 0+ (optional)  | Float         | The (average) incident energy that was used to measure or simulate the data of the ROOT file.                                                                                                                                                                                                                               |
| 2.6  | beamEnergyLower       | 0/1 (optional) | Float         | The lower threshold for incident energies that are used to measure or simulate the data of the ROOT file, i.e. the smallest incident energy.                                                                                                                                                                                |
| 2.7  | beamEnergyUpper       | 0/1 (optional) | Float         | The upper threshold for incident energies that are used to measure or simulate the data of the ROOT file, i.e. the largest incident energy.                                                                                                                                                                                 |
| 2.8  | beamEnergyUnits       | 0/1 (optional) | string        | The units in which beamEnergy is provided: eV or MeV/u; mandatory if beamEnergy, beamEnergyLower, or beamEnergyUpper is provided.                                                                                                                                                                                           |
| 2.9  | beamSource            | 0+ (optional)  | string        | The name of the instrument or facility producing the beamProbe.                                                                                                                                                                                                                                                             |
| 2.10 | beamSourceType        | 0+ (optional)  | string        | The type which produces the beamProbe, e.g. Spallation Neutron Source, Pulsed Reactor Neutron Source, Reactor Neutron Source, Synchrotron X-ray Source, Pulsed Muon Source, Rotating Anode X-ray, Fixed Tube X-ray, UV Laser, Free-Electron Laser, Optical Laser, Ion Source, UV Plasma Source, Metal Jet X-ray, Microtron. |

This list have to be extended by HEP community, e.g. NAPMIX.

## 3. ROOT Specific Metadata

The versatility of ROOT files requires metadata to support interoperability in sense of the FAIR principles. This metadata is intended to implement customized classes into the ROOT file which allows to setup a ROOT system to handle the data of the ROOT file.

| No.  | Name                  | Cardinality    | Format | Description                                                                                                                                                                                                                                                                             |
|------|-----------------------|----------------| ---    |-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 3.1  | operatingSystem       | 0+ (optional)  | string | An operating system that supports the required version of the ROOT system.                                                                                                                                                                                                              |
| 3.2  | operatingSystemDocker | 0+ (optional)  | string | A docker operating system image from [dockerhub](https://hub.docker.com/) that supports the required version of the ROOT system.                                                                                                                                                        |
| 3.3  | dockerShellScript     | 0/1 (optional) | string | A shell script to be executed within the docker image, e.g. to process a makefile or implement customized ROOT classes. If not defined, the field rootCommand could be used to provide ROOT specific commands that must be executed to grant access to the data.                        |
| 3.4  | rootVersion           | 0+ (optional)  | string | The version of a ROOT system that is able to handle the ROOT file, i.e. to process its data. For examples, see https://root.cern/install/all_releases/.                                                                                                                                 |
| 3.5  | customCodeFile        | 0+ (optional)  | string | The file content that defines classes or routines which are required to access or handle the data of the ROOT file (usually encoded in C++).                                                                                                                                            |
| 3.6  | customCodeFileName    | 0+ (optional)  | string | The name of the file whose content is provided in customCodeFile. Mandatory for each customCodeFile, e.g. customCodeFile_item1, customCodeFileName_item1, customCodeFile_item2, customCodeFileName_item2 etc. The file name should be the same as used in makeFile or cmakeFile script. |
| 3.7  | makeFile              | 0/1 (optional) | string | A MAKE file to incorporate customized code into the ROOT system. The MAKE script should work with customCodeFileName on the same level, i.e. no folder hierarchy should be included.                                                                                                    |
| 3.8  | cmakeFile             | 0/1 (optional) | string | A CMAKE file to incorporate customized code into the ROOT system. The CMAKE script should work with customCodeFileName on the same level, i.e. no folder hierarchy should be included.                                                                                                  |
| 3.9  | rootCommand           | 0/1 (optional) | string | A ROOT shell script command which is required to access data, e.g. in case of the A4 example the implementation of the TMedusa C++ class by 'gSystem->Load("libTMedusa")'.                                                                                                              |
| 3.10 | structureXsd          | 0/1 (optional) | string | An XSD file that defines the structure of the data within the ROOT file. It is intended to help humans or machines to navigate through the file.                                                                                                                                        |
| 3.11 | manual                | 0/1 (optional) | string | A description that helps to setup a ROOT system that can read and process the ROOT file, e.g. a step-wise instruction.                                                                                                                                                                  |



## 4. Simple Semantics

The structure of the Readme branch follows basic principles:

**Simple** - Only key-value pairs are allowed, i.e. a variable name containing a simple data type. Structs (combinations of different variable types) and arrays should be avoided to make the access as simple as possible.

**Optional** - All fields are optional since any information about the ROOT file is welcome.

**Human- & machine-readability** - Most basic fields are expected to contain arbitrary strings which is the easiest way for a scientist to provide information. Corresponding fields for web-resolvable PIDs (e.g. pointing to at term of an ontology) which are more precise for humans and machines can be created by adding a suffix (see below).

**Versatile** - Any field can be added to serve the needs of various communities which rely on ROOT files.

The combination of principles demands for minimal logic to be added to the field names which puts some constrains on field names and their extension with possible suffixes. 

### 4.1 Additional Field Names

The schema can be extended by own key-value pairs, e.g. to provide facility-internal experiment identifiers (proposal numbers) to which the ROOT file is assigned. Field names (keys) are allowed to only consist of characters [A-Z][a-z] and integers [0-9] in camelCase notation (no underscores).

### 4.2 Suffixes

Field names could be extended by suffixes to provide additional information. To avoid ambiguity, predefined suffixes start with a single underline and a key word, following the format _keyWord. The key word can consist of characters [A-Z][a-z] and integers [0-9] in camelCase notation (no underscores allowed beside that preceding the keyword). The single underscore is used to differ between field name and suffix.

### 4.3 Field Lists

The _item[INT] suffix realizes multiple fields of the same type (a potential list indicated by cardinality 0+) while keeping the structure of the schema flat: 

**_item[INT]** - Single elements of a list are numbered consecutively by suffixes starting with '_item' followed by an integer beginning with 0, e.g. contributor_item0, contributor_item1, contributor_item3...

A suffix can be neglected if only a single instance of a field exists, i.e. contributor can be used as a field name if only one contributor is provided. The only purpose of this suffix is to make field names of same type unique.

### 4.5 Data Types

**_dataType** - A string describing the C++ data type of the field's value, e.g. 'float', 'string', 'int'. This suffix is intended to improve the machine-readability for additional fields that are not defined by this schema.

### 4.6 Machine-Readable References

The fields of this schema are intended to be strings eligible to humans. Additional web-resolvable pointers can be provided by extending a field by the _seeAlso suffix which indicate a machine-ationable information.

**_seeAlso** - A web-resolvable pointer to a resource describing the field. For example, an ORCID could be provided for contributor_item0 by creating the field contributor_item0_seeAlso.

 A corresponding field without suffix doesn't have to exist, i.e. only a web-resolvable pointer could be provided.

### 4.7 Combinations of Suffixes

Combinations of different suffixes has to be added according to the order given. As a result, only one _seeAlso element can be added to define the content of a field, e.g. the additional field proposalNumber_dataType_seeAlso could contain a web-resolvable pointer to a term of an ontology which defines the data type of the field proposalNumber.
